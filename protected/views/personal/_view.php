<?php
/* @var $this PersonalController */
/* @var $data Personal */
?>

<div class="view">
	
	<?php echo $this->get_image($data->id."_small", $data->fullname, 100); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fullname')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->fullname), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('position')); ?>:</b>
	<?php echo CHtml::encode($data->position); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shortdscr')); ?>:</b>
	<?php echo CHtml::encode($data->shortdscr); ?>
	<br />

</div>