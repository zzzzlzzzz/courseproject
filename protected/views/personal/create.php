<?php
/* @var $this PersonalController */
/* @var $model Personal */

$this->breadcrumbs=array(
	'Персонал'=>array('index'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список Сотрудников', 'url'=>array('index')),
	array('label'=>'Управление Персоналом', 'url'=>array('admin')),
);
?>

<h1>Создание Сотрудника</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>