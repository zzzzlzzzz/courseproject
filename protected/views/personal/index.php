<?php
/* @var $this PersonalController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Персонал',
);

$this->menu=array(
	array('label'=>'Создать Сотрудника', 'url'=>array('create')),
	array('label'=>'Управление Персоналом', 'url'=>array('admin')),
);
?>

<script type="text/javascript">
	function jumpUp()
	{
		var elid = 0;
		
		while(document.getElementById(elid) && document.getElementById(elid).style.display == "none")
			++elid;
			
		var startShowDiv = elid;
		
		while(document.getElementById(elid) && document.getElementById(elid).style.display != "none")
			++elid;
			
		var endShowDiv = elid - 1;
		
		while(document.getElementById(elid))
			++elid;
			
		var totalElements = elid - 1;
		
		if(startShowDiv > 0)
		{
			document.getElementById(startShowDiv - 1).style.display = "block";
			document.getElementById(endShowDiv).style.display = "none";
		}
	}
	
	function jumpDown()
	{
		var elid = 0;
		
		while(document.getElementById(elid) && document.getElementById(elid).style.display == "none")
			++elid;
			
		var startShowDiv = elid;
		
		while(document.getElementById(elid) && document.getElementById(elid).style.display != "none")
			++elid;
			
		var endShowDiv = elid - 1;
		
		while(document.getElementById(elid))
			++elid;
			
		var totalElements = elid - 1;
		
		if(endShowDiv < totalElements)
		{
			document.getElementById(startShowDiv).style.display = "none";
			document.getElementById(endShowDiv + 1).style.display = "block";
		}
	}
</script>

<h1>Персонал</h1>

<div id="personalView">
	<div onclick="jumpUp();" style="border: 1px dotted black; clear: left; -moz-user-select: none; -webkit-user-select: none; -ms-user-select: none; user-select: none;">
		<img style="display: block;margin: 0 auto;" width=20 src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/goup.gif">
	</div>
	<?php
		$personals = Personal::model()->findAll();
	
		$idno = 0;
		foreach($personals as $person)
		{
			$findPath = glob(dirname(rtrim($_SERVER['DOCUMENT_ROOT'],'/').Yii::app()->urlManager->baseUrl).'/uploads/'.$person->id.'_small.*');
			$imgPath = './uploads/'.(count($findPath) > 0? basename($findPath[0]) : "no_photo.gif");
			
			?>
				<div id="<?php echo $idno++; ?>" style="<?php if($idno > 3) echo "display: none;"; ?>clear: left; margin: 10px; height: 170px; border: 1px solid black;">
					<div style="float: left; margin: 10px; width: 20%;">
						<?php
							echo CHtml::link(CHtml::image($imgPath, $person->fullname, array("width"=>"100")), array('view', 'id'=>$person->id));
						?>
					</div>
					<div style="float: left; margin: 10px; width: 70%;">
						<b><?php echo CHtml::encode($person->getAttributeLabel('fullname')); ?>:</b>
						<?php echo CHtml::link(CHtml::encode($person->fullname), array('view', 'id'=>$person->id)); ?>
						<br />
							
						<b><?php echo CHtml::encode($person->getAttributeLabel('position')); ?>:</b>
						<?php echo CHtml::encode($person->position); ?>
						<br />
							
						<b><?php echo CHtml::encode($person->getAttributeLabel('shortdscr')); ?>:</b>
						<?php echo CHtml::encode($person->shortdscr); ?>
						<br />
					</div>
				</div>
			<?php
		}
	?>
	<div onclick="jumpDown();" style="border: 1px dotted black; clear: left; -moz-user-select: none; -webkit-user-select: none; -ms-user-select: none; user-select: none;">
		<img style="display: block;margin: 0 auto;" width=20 src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/godown.gif">
	</div>
</div>
