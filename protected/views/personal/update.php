<?php
/* @var $this PersonalController */
/* @var $model Personal */

$this->breadcrumbs=array(
	'Персонал'=>array('index'),
	'Изменить',
);

$this->menu=array(
	array('label'=>'Список Сотрудников', 'url'=>array('index')),
	array('label'=>'Создать Сотрудника', 'url'=>array('create')),
	array('label'=>'Просмотр Сотрудника', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление Персоналом', 'url'=>array('admin')),
);
?>

<h1>Изменить Сотрудника <?php echo $model->fullname; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>