<?php
/* @var $this PersonalController */
/* @var $model Personal */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'personal-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'fullname'); ?>
		<?php echo $form->textField($model,'fullname',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'fullname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'position'); ?>
		<?php echo $form->textField($model,'position',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'position'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'shortdscr'); ?>
		<?php echo $form->textArea($model,'shortdscr',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'shortdscr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'longdscr'); ?>
		<?php echo $form->textArea($model,'longdscr',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'longdscr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'smallImageValue'); ?>
		<?php echo $this->get_image($model->id."_small", $model->fullname, 100, 'my') ?>
		<?php
			//Если картинка для данного товара загружена, 
			//предложить её удалить отметив чекбокс
			if(count(glob(dirname(rtrim($_SERVER['DOCUMENT_ROOT'],'/').Yii::app()->urlManager->baseUrl).'/uploads/'.$model->id.'_small.*')))
			{
				echo '<div class="row">';
				echo $form->labelEx($model,'deleteSmallImageValue'); 
				echo $form->checkBox($model,'deleteSmallImageValue' );   
				echo '</div>';
			}
		?> 
		<?php echo CHtml::activeFileField($model, 'smallImageValue'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'bigImageValue'); ?>
		<?php echo $this->get_image($model->id."_big", $model->fullname, 100, 'my') ?>
		<?php
			//Если картинка для данного товара загружена, 
			//предложить её удалить отметив чекбокс
			if(count(glob(dirname(rtrim($_SERVER['DOCUMENT_ROOT'],'/').Yii::app()->urlManager->baseUrl).'/uploads/'.$model->id.'_big.*')))
			{
				echo '<div class="row">';
				echo $form->labelEx($model,'deleteBigImageValue'); 
				echo $form->checkBox($model,'deleteBigImageValue' );   
				echo '</div>';
			}
		?> 
		<?php echo CHtml::activeFileField($model, 'bigImageValue'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'anketaFileValue'); ?>
		<?php
			$findPath = glob(dirname(rtrim($_SERVER['DOCUMENT_ROOT'],'/').Yii::app()->urlManager->baseUrl).'/uploads/'.$model->id.'_anketa.*');
			if(count($findPath))
			{
				
				echo CHtml::link('Скачать', CHtml::normalizeUrl('./uploads/'.basename($findPath[0])));
				echo '<br><div class="row">';
				echo $form->labelEx($model,'deleteAnketaFileValue'); 
				echo $form->checkBox($model,'deleteAnketaFileValue' );   
				echo '</div>';
			}
		?> 
		<?php echo CHtml::activeFileField($model, 'anketaFileValue'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->