<?php
/* @var $this PersonalController */
/* @var $model Personal */

$this->breadcrumbs=array(
	'Персонал'=>array('index'),
	$model->fullname,
);

$this->menu=array(
	array('label'=>'Список Сотрудников', 'url'=>array('index')),
	array('label'=>'Создать Сотрудника', 'url'=>array('create')),
	array('label'=>'Изменить Сотрудника', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить Сотрудника', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление Персоналом', 'url'=>array('admin')),
);
?>

<h1>Просмотр анкеты <?php echo $model->fullname; ?></h1>

<?php 
	$findPath = glob(dirname(rtrim($_SERVER['DOCUMENT_ROOT'],'/').Yii::app()->urlManager->baseUrl).'/uploads/'.$model->id.'_anketa.*');

	$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
			'label'=>$model->attributeLabels()['bigImageValue'],
			'type'=>'raw',
			'value'=> $this->get_image($model->id."_big", $model->fullname, 300),
		),
		'fullname',
		'position',
		array(
			'label'=>$model->attributeLabels()['anketaFileValue'],
			'type'=>'raw',
			'value'=> count($findPath) > 0?CHtml::link('Скачать', CHtml::normalizeUrl('./uploads/'.basename($findPath[0]))) : "Не загружена",
		),
		'longdscr',
	),
)); ?>
