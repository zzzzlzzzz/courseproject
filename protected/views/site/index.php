<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Добро Пожаловать <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>Поздравляем! Вы успешно создали своё Yii приложение.</p>

<p>Вы можете изменить содержимое этой страницы, изменяя следующие два файла:</p>
<ul>
	<li>View файл: <code><?php echo __FILE__; ?></code></li>
	<li>Layout файл: <code><?php echo $this->getLayoutFile('main'); ?></code></li>
</ul>

<p>
Для более подробной информации о том, как дальше развивать это приложение, пожалуйста, прочитайте <a href="http://www.yiiframework.com/doc/">документацию</a>.
Интересующие вопросы задавайте на <a href="http://www.yiiframework.com/forum/">форуме</a>.</p>
