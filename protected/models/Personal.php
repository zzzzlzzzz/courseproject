<?php

/**
 * This is the model class for table "tbl_personal".
 *
 * The followings are the available columns in table 'tbl_personal':
 * @property integer $id
 * @property string $fullname
 * @property string $position
 * @property string $shortdscr
 * @property string $longdscr
 */
class Personal extends CActiveRecord
{
	public $smallImageValue;
	public $deleteSmallImageValue;
	public $bigImageValue;
	public $deleteBigImageValue;
	public $anketaFileValue;
	public $deleteAnketaFileValue;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_personal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fullname, position', 'required'),
			array('fullname, position', 'length', 'max'=>255),
			array('shortdscr, longdscr', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fullname, position, shortdscr, longdscr', 'safe', 'on'=>'search'),
			// images
			array('deleteSmallImageValue', 'boolean'),
			array('deleteBigImageValue', 'boolean'),
			array('deleteAnketaFileValue', 'boolean'),
			array	('smallImageValue', 'file',
					'types'=>'jpg, gif, png',
					'maxSize'=>1024 * 1024 * 5, // 5MB
					'allowEmpty'=>'true',
					'tooLarge'=>'The file was larger than 5MB. Please upload a smaller file.',
					),
			array	('bigImageValue', 'file',
					'types'=>'jpg, gif, png',
					'maxSize'=>1024 * 1024 * 5, // 5MB
					'allowEmpty'=>'true',
					'tooLarge'=>'The file was larger than 5MB. Please upload a smaller file.',
					),
			array	('anketaFileValue', 'file',
					'types'=>'pdf, doc, txt',
					'maxSize'=>1024 * 1024 * 5, // 5MB
					'allowEmpty'=>'true',
					'tooLarge'=>'The file was larger than 5MB. Please upload a smaller file.',
					),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Идентификатор',
			'fullname' => 'ФИО',
			'position' => 'Должность',
			'shortdscr' => 'Краткое описание',
			'longdscr' => 'Полное описание',
			'smallImageValue' => 'Миниатюра',
			'bigImageValue' => 'Фотография',
			'anketaFileValue' => 'Анкета',
			'deleteSmallImageValue' => 'Удалить миниатюру',
			'deleteBigImageValue' => 'Удалить фотографию',
			'deleteAnketaFileValue' => 'Удалить анкету',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fullname',$this->fullname,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('shortdscr',$this->shortdscr,true);
		$criteria->compare('longdscr',$this->longdscr,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Personal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
